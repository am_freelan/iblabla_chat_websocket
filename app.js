const app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const {Pool} = require('pg');
let bodyParser = require('body-parser');
let config = require('./config');
let lessonMessageService = require('./services/lessonMessageService');
let interviewMessageService = require('./services/interviewMessageService');
const connectionString = config.database.pgConnection

const pool = new Pool({
    connectionString: connectionString,
})

const PORT = config.server.port;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use((req, res, next) => {
    console.log(`URL: ${req.originalUrl} BODY: ${JSON.stringify(req.body)}`);
    next();
});

app.get((req, res) => {
   return res.send({info: 'Iblabla websocket server for lesson and interview chats'})
});

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('chat_lesson', (msg) => {
        // console.log('chat_lesson', msg);
        io.emit('chat_lesson.'+msg.lesson_id, msg);
        lessonMessageService.save(pool, msg)
    });

    socket.on('chat_interview', (msg) => {
        console.log('chat_interview', msg);
        io.emit('chat_interview.'+msg.interview_id, msg);
        interviewMessageService.save(pool, msg)
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});

http.listen(PORT, () => console.log(`Server has started on ${PORT}`));