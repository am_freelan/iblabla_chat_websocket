exports.save = async (pool, data) => {
    const text = 'INSERT INTO interview_message(interview_id, user_id, message, created_at, updated_at) VALUES($1, $2, $3, $4, $5) RETURNING *'
    const dateNow = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
    const values = [data.interview_id, data.user_id, data.message, dateNow, dateNow];

    return await pool.query(text, values)
        .then(res => res.rows[0])
        .catch(err => console.error('Error executing query', err.stack))
};